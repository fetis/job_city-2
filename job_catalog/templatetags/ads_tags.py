from django import template

from job_catalog.models import Category, Vacancy, CV

register = template.Library()


@register.assignment_tag
def get_categories():
    categories = list(Category.objects.get_categories())
    return {'count': len(categories), 'cat_list': categories}


@register.simple_tag
def get_vacancy_count():
    return Vacancy.objects.get_vacancy_count()


@register.simple_tag
def get_cv_count():
    return CV.objects.get_cv_count()


@register.inclusion_tag('_cv_list_search.html', takes_context=True)
def render_cv_list(context, cv_list):
    return {'list': cv_list, 'MEDIA_URL': context['MEDIA_URL'], 'STATIC_URL': context['STATIC_URL'],
            'request': context['request']}


@register.inclusion_tag('_vacancy_list_search.html', takes_context=True)
def render_vacancy_list(context, vacancy_list):
    return {'list': vacancy_list, 'MEDIA_URL': context['MEDIA_URL'], 'STATIC_URL': context['STATIC_URL'],
            'request': context['request']}

@register.assignment_tag
def get_favorites(user):
    if user.is_authenticated:
        return user.get_favorites_ids()
    else:
        return None