from django.core.management.base import BaseCommand

from job_catalog.models import VacancySync


class Command(BaseCommand):
    help = 'Удаление синхронизированных файлов'

    def handle(self, *args, **options):
        sync = VacancySync.objects.filter(status=3)
        if sync.count() >= 10:
            sync.delete()