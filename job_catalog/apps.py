from django.apps import AppConfig


class JobCatalogConfig(AppConfig):
    name = 'job_catalog'
    verbose_name = 'Вакансии и резюме'
