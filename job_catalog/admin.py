from django.contrib import admin

from .models import CV, Vacancy, Category, VacancySync, VacancyRequest


# Register your models here.


class AdvertsAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'status', 'in_top']
    list_filter = ['status']
    actions = ['allowed']
    readonly_fields = ['view_count']

    def get_status(self, obj):
        return obj.get_status_display()

    get_status.short_description = 'Статус'

    def allowed(self, request, queryset):
        queryset.update(status=2)
    allowed.short_description = "Активировать"


class VacancySyncAdmin(admin.ModelAdmin):
    readonly_fields = ['datetime_sync', 'log', 'status']


admin.site.register(CV, AdvertsAdmin)
admin.site.register(Vacancy, AdvertsAdmin)
admin.site.register(Category)
admin.site.register(VacancySync, VacancySyncAdmin)
admin.site.register(VacancyRequest)