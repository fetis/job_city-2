from django.conf.urls import url

from poll.views import PollIndexView, VoteAddView, AllPollListView

urlpatterns = [
    url(r'^$', PollIndexView.as_view(), name='index'),
    url(r'^all_polls$', AllPollListView.as_view(), name='polls_list'),
    url(r'^vote/(?P<pk>\d+)/$', VoteAddView.as_view(), name='vote'),
]