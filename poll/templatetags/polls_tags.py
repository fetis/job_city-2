from django import template

from poll.models import Poll
from poll.utils import get_votes_cookies

register = template.Library()


@register.inclusion_tag('polls/_poll_detail_form.html')
def render_poll_detail_form(poll, show_link=True):
    return {'poll': poll, 'show_link': show_link}


@register.inclusion_tag('polls/_poll_complete_detail_form.html')
def render_poll_detail_result(poll):
    return {'poll': poll}


@register.inclusion_tag('polls/_poll_for_footer.html')
def render_poll_in_footer(request):
    polls_complete = get_votes_cookies(request.COOKIES.get('polls_vote', []))
    active_polls = Poll.objects.get_active_polls().prefetch_related('votes')
    active_poll = active_polls.exclude(pk__in=polls_complete).first()
    if active_poll:
        data = {'poll': active_poll, 'view': 'form'}
    else:
        result_poll = active_polls.first()
        data = {'poll': result_poll, 'view': 'result'}
    return data


@register.assignment_tag
def is_vote(poll_pk, request):
    polls_complete = get_votes_cookies(request.COOKIES.get('polls_vote', []))
    return poll_pk in polls_complete
