# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-02-13 16:19
from __future__ import unicode_literals

from django.db import migrations, models
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0012_newspaper'),
    ]

    operations = [
        migrations.CreateModel(
            name='PriceList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Название прайса')),
                ('preview', sorl.thumbnail.fields.ImageField(upload_to='price_list_preview', verbose_name='Превью прайса')),
                ('price_file', models.FileField(upload_to='', verbose_name='Файл прайса')),
            ],
            options={
                'verbose_name_plural': 'Прайсы',
                'verbose_name': 'Прайс',
            },
        ),
    ]
