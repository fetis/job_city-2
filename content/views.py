import datetime

from django.contrib import messages
from django.http import Http404
from django.urls import reverse_lazy, reverse
from django.utils.formats import date_format
from django.utils.text import Truncator
from django.views.generic import ListView, DetailView, TemplateView, CreateView
from django.views.generic.base import View
from pure_pagination import PaginationMixin
from watson.views import SearchView

from content.models import Feedback, Partner, Education, CongratulationsPartner, Article, Action, QAJurist, NewsPaper, \
    PriceList, News, SalaryStat, ExpertNote, Event
from job.utils import json_response, MenuMixin
from job_catalog.models import Category


class FeedBackListView(MenuMixin, PaginationMixin, ListView):
    paginate_by = 10
    template_name = 'content/feedback_list.html'
    queryset = Feedback.objects.all().order_by('-created')
    context_object_name = 'feedback_list'
    menu_slug = 'feed'


class PartnerListView(MenuMixin, PaginationMixin, ListView):
    paginate_by = 12
    template_name = 'content/partner_list.html'
    queryset = Partner.objects.all()
    context_object_name = 'partner_list'
    menu_slug = 'partners'


class EducationAdvertsListView(MenuMixin, PaginationMixin, ListView):
    paginate_by = 10
    template_name = 'content/edu_list.html'
    queryset = Education.objects.filter(active=True)
    context_object_name = 'edu_list'
    menu_slug = 'edu'


class FullSearchView(SearchView):
    template_name = "search/search_results.html"

    def get_models(self):
        return (Education.objects.filter(active=True),
                Partner.objects.all(),
                Feedback.objects.all())


class BirthDaysView(MenuMixin, PaginationMixin, ListView):
    paginate_by = 200
    template_name = 'content/birth_day_list.html'
    menu_slug = 'birthday'

    def get_verbose_month(self):
        today = datetime.date.today()
        if today.month == 1:
            return 'январе'
        elif today.month == 2:
            return 'феврале'
        elif today.month == 3:
            return 'марте'
        elif today.month == 4:
            return 'апреле'
        elif today.month == 5:
            return 'мае'
        elif today.month == 6:
            return 'июне'
        elif today.month == 7:
            return 'июле'
        elif today.month == 8:
            return 'августе'
        elif today.month == 9:
            return 'сентябре'
        elif today.month == 10:
            return 'октябре'
        elif today.month == 11:
            return 'ноябре'
        elif today.month == 12:
            return 'декабре'

    def get_queryset(self):
        today = datetime.date.today()
        objects = CongratulationsPartner.objects.filter(birth_day__month=today.month)
        return objects

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['month'] = self.get_verbose_month()
        return ctx


class ArticleListView(MenuMixin, PaginationMixin, ListView):
    paginate_by = 10
    queryset = Article.objects.filter(published=True)
    template_name = 'content/article_list.html'
    menu_slug = 'posts'


class ArticleDetailView(MenuMixin, DetailView):
    template_name = 'content/article_detail.html'
    queryset = Article.objects.filter(published=True)
    menu_slug = 'posts'

    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        obj.views += 1
        obj.save(update_fields=['views'])
        return obj


class ActionListView(MenuMixin, PaginationMixin, ListView):
    template_name = 'content/actions.html'
    paginate_by = 10
    queryset = Action.objects.filter(published=True)
    menu_slug = 'actions'


class ContactView(MenuMixin, TemplateView):
    template_name = 'content/contacts.html'
    menu_slug = 'contact'


class QAJuristDetail(MenuMixin, TemplateView):
    template_name = 'content/qa_detail.html'
    menu_slug = 'qa'

    def get_queryset(self):
        if self.kwargs.get('pk'):
            try:
                qs = QAJurist.objects.get_published_qa().get(pk=self.kwargs['pk'])
            except QAJurist.DoesNotExist:
                raise Http404
        else:
            qs = QAJurist.objects.get_published_qa().first()
            if not qs:
                return QAJurist.objects.none()
        return qs

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['object'] = self.get_queryset()
        ctx['years'] = QAJurist.objects.get_years()
        ctx['qa_list'] = QAJurist.objects.get_qa_current_year()
        return ctx


class QAJuristCreateView(MenuMixin, CreateView):
    template_name = 'content/qa_form.html'
    model = QAJurist
    fields = ['question']
    success_url = reverse_lazy('content:qa_detail')
    menu_slug = 'qa'

    def form_valid(self, form):
        messages.add_message(self.request, messages.INFO, 'Вопрос успешно создан!')
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['years'] = QAJurist.objects.get_years()
        ctx['qa_list'] = QAJurist.objects.get_qa_current_year()
        return ctx


class QAJuristQuestionAjax(View):

    def get(self, request, *args, **kwargs):
        data = list()
        questions = QAJurist.objects.get_year_answers(kwargs.get('year')).values('created', 'question', 'pk')
        for question in questions:
            truncated_text = Truncator(question['question']).words(10)
            link = reverse('content:qa_detail', kwargs={'pk': question['pk']})
            q = {'created': date_format(question['created'], 'd E Y'), 'link': link, 'text': truncated_text}
            data.append(q)
        return json_response(data)


class NewsPaperList(MenuMixin, PaginationMixin, ListView):
    template_name = 'content/news_paper_list.html'
    queryset = NewsPaper.objects.all()
    paginate_by = 9
    menu_slug = 'archive'


class PriceListView(MenuMixin, PaginationMixin, ListView):
    template_name = 'content/price_list.html'
    queryset = PriceList.objects.all()
    paginate_by = 6
    menu_slug = 'prices'


class NewsList(MenuMixin, PaginationMixin, ListView):
    template_name = 'content/news_list.html'
    queryset = News.objects.filter(published=True)
    paginate_by = 10
    menu_slug = 'news'


class NewsDetail(MenuMixin, DetailView):
    template_name = 'content/news.html'
    queryset = News.objects.filter(published=True)
    paginate_by = 10
    menu_slug = 'news'


class SalaryStatList(MenuMixin, PaginationMixin, ListView):
    template_name = 'content/salary_stat_list.html'
    queryset = SalaryStat.objects.all()
    years = None
    menu_slug = 'stat'

    def get_queryset(self):
        self.years = SalaryStat.objects.get_years()
        if self.kwargs.get('year'):
            year = self.kwargs.get('year')
        elif self.years.exists():
            year = self.years.first().year
        else:
            year = datetime.date.today().year
        category_pk = None
        if self.request.GET.get('category') and self.request.GET['category'].isdigit():
            category_pk = self.request.GET['category']
        return SalaryStat.objects.search(year, category=category_pk, profession=self.request.GET.get('profession'))

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['years'] = self.years
        ctx['categories'] = Category.objects.all()
        ctx['current_year'] = self.kwargs.get('year')
        return ctx


class ExpertNoteListView(PaginationMixin, ListView):
    template_name = 'content/expert_note_list.html'
    queryset = ExpertNote.objects.all()
    paginate_by = 20


class ExpertNoteDetailView(DetailView):
    template_name = 'content/expert_note.html'
    queryset = ExpertNote.objects.all()


class EventList(PaginationMixin, ListView):
    template_name = 'content/event_list.html'
    queryset = Event.objects.filter(published=True)
    paginate_by = 20


class EventDetail(DetailView):
    template_name = 'content/event.html'
    queryset = Event.objects.filter(published=True)
