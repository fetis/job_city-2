from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from users.forms import SubscribeTeplateAdminForm, EmployerProfileFormAdmin
from users.models import User, ApplicantProfile, EmployerProfile, SubscribeTemplate


class ApplicantProfileInline(admin.StackedInline):
    model = ApplicantProfile


class EmployerProfileInline(admin.StackedInline):
    model = EmployerProfile
    form = EmployerProfileFormAdmin


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        ('Персональная информация', {'fields': ('email', 'password')}),
        ('Права', {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        ('Прочая информация', {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'is_staff')
    ordering = ('email',)
    inlines = [ApplicantProfileInline, EmployerProfileInline]


class SubscribeTemplateAdmin(admin.ModelAdmin):
    form = SubscribeTeplateAdminForm


class EmployerAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'employer_day']
    form = EmployerProfileFormAdmin


admin.site.register(User, CustomUserAdmin)
admin.site.register(ApplicantProfile)
admin.site.register(EmployerProfile, EmployerAdmin)
admin.site.register(SubscribeTemplate, SubscribeTemplateAdmin)
