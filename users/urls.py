from django.conf.urls import url

from users.views import ProfileView, ProfileChangePhoto, ProfileUpdate, SubscribeCreateView, MySubscribesListView, \
    MySubscribeDeleteView

urlpatterns = [
    url(r'^$', ProfileView.as_view(), name='profile'),
    url(r'^update_profile/$', ProfileUpdate.as_view(), name='profile_update'),
    url(r'^set_photo/$', ProfileChangePhoto.as_view(), name='profile_set_photo'),
    url(r'^create_subscribe/$', SubscribeCreateView.as_view(), name='create_subscribe'),
    url(r'^my_subscribes/$', MySubscribesListView.as_view(), name='my_subscribes'),
    url(r'^delete_subscribe/(?P<pk>\d+)/$', MySubscribeDeleteView.as_view(), name='delete_subscribe'),
]