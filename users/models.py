from datetime import date

from ckeditor.fields import RichTextField
from django.contrib.auth.models import (AbstractBaseUser, PermissionsMixin)
from django.contrib.auth.models import UserManager
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.db import models
from django.db.models import EmailField
from django.template import Template, Context
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from sorl.thumbnail import ImageField
from django.utils import timezone

from .tasks import subscribe_send_mail

SUBSCRIBE_TYPE = (
    (0, 'Вакансии'),
    (1, 'Резюме')
)


class User(AbstractBaseUser, PermissionsMixin):
    email = EmailField(verbose_name='Email', unique=True)

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    favorites_vacancies = models.ManyToManyField('job_catalog.Vacancy', verbose_name='Избранные вакансии',
                                                 related_name='fav_vac', null=True)
    favorites_cv = models.ManyToManyField('job_catalog.CV', verbose_name='Избранные резюме', related_name='fav_cv',
                                          null=True)

    USERNAME_FIELD = 'email'

    objects = UserManager()

    def get_favorite_vacancies(self):
        return self.favorites_vacancies.filter(status=2, profile__user__is_active=True)

    def get_favorite_cv(self):
        return self.favorites_cv.filter(status=2, profile__user__is_active=True)

    def set_favorites_ids(self):
        fav_cv_ids = self.get_favorite_cv().values_list('pk', flat=True)
        fav_vac_ids = self.get_favorite_vacancies().values_list('pk', flat=True)
        if fav_vac_ids or fav_cv_ids:
            data = {'cv': fav_cv_ids, 'vac': fav_vac_ids}
        else:
            data = {}
        cache.set(settings.FAVORITE_ID_LIST_USER.format(self.pk), data, 3600)
        return data

    def get_favorites_ids(self):
        data = cache.get(settings.FAVORITE_ID_LIST_USER.format(self.pk), None)
        if data is not None:
            return data
        else:
            return self.set_favorites_ids()

    def set_favorite_count(self):
        cvs = self.get_favorite_cv().count()
        vacancy = self.get_favorite_vacancies().count()
        result = cvs + vacancy
        cache.set(settings.FAVORITE_COUNT_USER.format(self.pk), result, 3600)
        return result

    def get_favorite_count(self):
        ad_count = cache.get(settings.FAVORITE_COUNT_USER.format(self.pk), None)
        if ad_count is not None:
            return ad_count
        else:
            return self.set_favorite_count()

    @property
    def profile_employer(self):
        try:
            profile = self.employer_profile
            return profile
        except ObjectDoesNotExist:
            return None

    @property
    def profile_applicant(self):
        try:
            profile = self.applicant_profile
            return profile
        except ObjectDoesNotExist:
            return None

    def email_user(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_short_name(self):
        return self.email

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class EmployerProfile(models.Model):
    phone = models.CharField(verbose_name='Телефон', max_length=20, null=True, blank=True)
    logo = ImageField(verbose_name='Фото', null=True, blank=True, upload_to='logos')
    company = models.CharField(verbose_name='Название компании', max_length=255, null=True, blank=True)
    contact = models.CharField(verbose_name='Контактное лицо', max_length=255, null=True, blank=True)
    address = models.CharField(verbose_name='Адрес компании', max_length=300)
    user = models.OneToOneField(User, verbose_name='Пользователь', related_name='employer_profile')
    employer_day = models.BooleanField(verbose_name='Работодатель дня', default=False)

    def __str__(self):
        return self.user.email

    class Meta:
        verbose_name = 'Профиль работодателя'
        verbose_name_plural = 'Профили работодателей'


class ApplicantProfile(models.Model):
    phone = models.CharField(verbose_name='Телефон', max_length=20, null=True, blank=True)
    photo = ImageField(verbose_name='Фото', null=True, blank=True, upload_to='logos')
    first_name = models.CharField(verbose_name='Имя', max_length=255, null=True, blank=True)
    last_name = models.CharField(verbose_name='Фамилия', max_length=255, null=True, blank=True)
    third_name = models.CharField(verbose_name='Отчество', max_length=255, null=True, blank=True)
    birth_day = models.DateField(verbose_name='День рождения', null=True, blank=True)
    user = models.OneToOneField(User, verbose_name='Пользователь', related_name='applicant_profile')

    def __str__(self):
        return self.user.email

    def years(self):
        today = date.today()
        return today.year - self.birth_day.year - ((today.month, today.day) < (self.birth_day.month,
                                                                               self.birth_day.day))

    class Meta:
        verbose_name = 'Профиль соискателя'
        verbose_name_plural = 'Профили соискателей'


class Subscribe(models.Model):
    user = models.ForeignKey(User, verbose_name='Пользователь', related_name='subscribes')
    category = models.ForeignKey('job_catalog.Category', verbose_name='Категория')
    subscribe_type = models.SmallIntegerField(verbose_name='Тип подписки', choices=SUBSCRIBE_TYPE, default=0)

    def send_message(self, adv_list):
        if adv_list:
            if self.subscribe_type == 0:
                context = {'vac_list': adv_list, 'category': self.category.name if self.category else None}
                subject = 'Новые вакансии по вашей подписке, на сайте "Кадры города"'
            else:
                context = {'cv_list': adv_list, 'category': self.category.name if self.category else None}
                subject = 'Новые резюме по вашей подписке, на сайте "Кадры города"'
            template = SubscribeTemplate.objects.filter(slug=self.subscribe_type).first()
            if template:
                tpl = template.create_template(context)
                subscribe_send_mail.delay(subject, tpl, self.user.email)


class SubscribeTemplate(models.Model):
    slug = models.SmallIntegerField(verbose_name='Тип подписки', choices=SUBSCRIBE_TYPE, default=0, unique=True)
    template = RichTextField(verbose_name='Шаблон')
    description = models.TextField(verbose_name='Описание')

    def __str__(self):
        return self.get_slug_display()

    def create_template(self, ctx):
        tpl = Template(self.template)
        template = tpl.render(Context(ctx))
        return template

    class Meta:
        verbose_name = 'Шаблон рассылки'
        verbose_name_plural = 'Шаблоны рассылки'
