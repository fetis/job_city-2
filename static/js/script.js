jQuery(document).ready(function () {

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    
    function get_info(is_cv, pk, obj) {
        if (is_cv){
            $.get('/get_cv_info/'+pk+'/', function(html){
                obj.append(html)
            })
        }else {
            $.get('/get_vacancy_info/' + pk + '/', function (html) {
                obj.append(html)
            })
        }
        
    }
    
    jQuery('.show_more_ol').on('click', function () {
        var this_ad = jQuery(this).closest('.one_line');
        var pk = jQuery(this).data('pk');
        var cv = jQuery(this).data('type');
        var is_cv = cv === 1;
        if (this_ad.find('.full_descr').css('display') === 'none') {
            var full_descr = this_ad.find('.full_descr');
            get_info(is_cv, pk, full_descr);
            full_descr.show('medium');
            jQuery(this).html('Скрыть обратно');
            jQuery(this).css('background-image', 'url("/static/img/arr_up.png")');
        } else {
            full_descr = this_ad.find('.full_descr');
            full_descr.empty();
            full_descr.hide('medium');
            jQuery(this).html('Посмотреть полностью');
            jQuery(this).css('background-image', 'url("/static/img/arr_dwn.png")');
        }

    });
    jQuery('.progress_bar').each(function () {
        jQuery(this).css('width', jQuery(this).attr('data-size'));
    });
    jQuery('.js_show_list').on('click', function () {
        jQuery(this).next('.js_list_hidden').show();
    });
    jQuery(document).mouseup(function (e){
		var div = jQuery(".js_list_hidden");
		if (!div.is(e.target)
		    && div.has(e.target).length === 0) {
			div.hide();
		}
	});
    jQuery(function(jQuery){
		// bind event handlers to modal triggers
		jQuery('body').on('click', '.js_trigger', function(e){
            e.preventDefault();
            jQuery('#test-modal').modal().open();
        });
		jQuery('body').on('click', '.js_trigger2', function(e){
			e.preventDefault();
			jQuery('#subscribe-modal').modal().open();
		});

		// attach modal close handler
		jQuery('.modal .close').on('click', function(e){
			e.preventDefault();
			jQuery.modal().close();
		});
	});
    $('[name="view"]').change(function () {
        var queryDict = {};
        console.log($(this).val());
        if($(this).val()){
            queryDict['view'] = $(this).val();
        }
        else{
            delete queryDict['view']
        }
        if($.isEmptyObject(queryDict)){
            window.location.replace("/");
        }else{
            var str = jQuery.param(queryDict);
            window.location.replace("/?" + str);
        }
    });
    $(document).on('click', '.to_fav', function(e) {
        e.preventDefault();
        var csrftoken = getCookie('csrftoken');
        var url = $(this).attr('href');
        var ob = $(this);
        var request = $.post(url, {'csrfmiddlewaretoken': csrftoken});
        request.done(function (data) {
            var count = data.count;
            var url = data.url;
            $('#fav_count').html(count);
            ob.removeClass('to_fav').addClass('delete_fav');
            ob.attr('href', url)
        });
    });
    $(document).on('click', '.delete_fav', function(e) {
        e.preventDefault();
        var csrftoken = getCookie('csrftoken');
        var url = $(this).attr('href');
        var ob = $(this);
        var request = $.post(url, {'csrfmiddlewaretoken': csrftoken});
        request.done(function (data) {
            var count = data.count;
            var url = data.url;
            $('#fav_count').html(count);
            ob.removeClass('delete_fav').addClass('to_fav');
            ob.attr('href', url)
        });
    });
    $(document).on('click', '.qa_year_link', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var request = $.get(url);
        $('.jur_lnk1').empty();
        request.done(function (data) {
            data.forEach(function (item) {
                var result = "<a href='"+item.link+"'>\n" +
                    "<p class=\"j_date\">"+item.created+"</p>\n" +
                    "<p class=\"j_intro\">"+item.text+"</p>\n" +
                    "</a>";
                $('.jur_lnk1').append(result);
                console.log(result)
            });
        });
    });
    $(document).on('click', '.quest_blc .btn_black', function(e) {
        var id = $(this).data('form-id');
        var form = $('#vote_form_'+id);
        var action = form.attr('action');
        var request = $.post(action, form.serialize());
        request.done(function (data) {
            if (data.result===true){
                $("#poll_card_"+id).remove();
                location.reload();
            }
        });
    });
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.scrollup').fadeIn();
        } else {
            jQuery('.scrollup').fadeOut();
        }
    });

    jQuery('.scrollup').click(function () {
        jQuery("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
});
